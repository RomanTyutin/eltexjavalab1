package com.eltex.lab1;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
